﻿using System.Collections;
using UnityEngine;

public class MovePlayer : MonoBehaviour{
    private Rigidbody2D rb;
    private float       futureTime;

    [SerializeField] [Tooltip("Отка блинка")]
    private float timeBlink = 3f;

    [SerializeField] [Tooltip("вес персонажа, чем меньше тем выше прыгнет")]
    private float jumpHeight = 2.4f;

    [SerializeField] [Tooltip("длинна на которую работает блинк")]
    private float blinkLong = 2f;

    [SerializeField] [Tooltip("точка респа")]
    private GameObject respPoint;

    private Effector _effector;
    private bool stop = false;

    private void Start() {
        rb              =  GetComponent<Rigidbody2D>();
        futureTime      = Time.time;
        rb.gravityScale =  jumpHeight;
        _effector = FindObjectOfType<Effector>();
    }

    void Update() {
print(futureTime);
        if(stop) {
            return;
        }
        
        RunPlayer();
        Blink();
        CheckGround();
    }

    private void RunPlayer() {
        float delta = Time.deltaTime;
        transform.position = new Vector2(transform.position.x + 3f * delta, transform.position.y);
    }

    private void Jump() {
        if(Input.GetKey(KeyCode.Space)) {
            rb.AddForce(Vector2.up, ForceMode2D.Impulse);
        }
    }

    private void CheckGround() {
        Vector2      pointCast = new Vector2(transform.position.x, transform.position.y - 0.5f);
        RaycastHit2D hit       = Physics2D.Raycast(pointCast, Vector3.down, 0.1f);
        Debug.DrawRay(pointCast, Vector3.down, Color.red, 0.1f);

        if(hit.collider != null) {
            if(hit.collider.CompareTag("Ground")) {
                Jump();
            }

            if(hit.collider.CompareTag("Death")) {
                StartCoroutine(Death());
            }
        }
    }

    private void Blink() {
        if(Time.time < futureTime) {
            return;
        }

        if(Input.GetKeyDown(KeyCode.LeftControl)) {
            futureTime = Time.time + timeBlink;
            print("блинк");
            if(_effector != null) {
                _effector.BlinkEffect(new Vector2(transform.position.x + blinkLong, transform.position.y));
                transform.position =  new Vector2(transform.position.x + blinkLong, transform.position.y);
            }
        }
    }

   IEnumerator Death() {
       stop = true;
       GetComponent<SpriteRenderer>().enabled = false;
       print("death");
        _effector.DeathEffect(transform.position);
        yield return new WaitForSeconds(1f);
        transform.position = respPoint.transform.position;
        GetComponent<SpriteRenderer>().enabled = true;
        stop = false;
   }
}

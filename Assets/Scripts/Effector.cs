﻿using UnityEngine;

public class Effector : MonoBehaviour
{

    [SerializeField] private GameObject deathEffect;
    [SerializeField] private GameObject blinkeffect;
    
    public void DeathEffect(Vector2 effectPosition) {
        Instantiate(deathEffect, effectPosition, Quaternion.identity);
    }

    public void BlinkEffect(Vector2 effectPosition) {
        Instantiate(blinkeffect, effectPosition, Quaternion.identity);
    }
    
}
